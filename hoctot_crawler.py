import sys
from csv import DictWriter
from pathlib import Path
import time
import cloudscraper
import pandas as pd
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
import itertools
import getopt
from dotenv import load_dotenv
import os
import logging
logging.basicConfig(filename='hoctot_crawler_log.log', level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

FOLDER = "hoctot"
list_subject = ["toan", "vat-ly", "hoa-hoc", "tieng-anh"]
exam_root_url = "https://hoctot.com/luyen-thi/luyen-thi-thpt-quoc-gia/lam-de"
load_dotenv()
HOCTOT_CAT_ID = os.environ.get("HOCTOT_CAT_ID")


class HoctotCrawler:
    def __init__(self):
        pass

    def get_course(self, cat_id):
        url = "https://hoctot.com/luyen-thi/get-courses-by-categories-id-at-category-page"
        scraper = cloudscraper.create_scraper()
        form_data = {
            'type': 0,
            'categoryId': cat_id,
            'offset': 0,
            'limit': -1
        }
        headers = {
            'Accept': '*/*',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        }

        response = scraper.post(
            url,
            data=form_data,
            headers=headers,
        )

        try:
            response_text = json.loads(response.text)
            courses = response_text["courses"]
            courses_selected = [x for x in courses if x['friendlyUrl'] in list_subject]
            return courses_selected
        except Exception as e:
            logging.debug('Error {} when get course onluyen'.format(str(e)))

    def get_exam_by_course_id(self, course_id, page=1):
        url = 'https://hoctot.com/luyen-thi/list-exam-by-course-id'
        scraper = cloudscraper.create_scraper()
        form_data = {
            'currentPage': page,
            'courseId': course_id,
            'offset': 0,
            'limit': 1000
        }
        headers = {
            'Accept': '*/*',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        }
        response = scraper.post(
            url,
            data=form_data,
            headers=headers,
        )

        try:
            response_text = json.loads(response.text)
            exams = response_text["items"]
            return exams
        except Exception as e:
            logging.debug('Error {} when get exam onluyen'.format(str(e)))


def append_dict_as_row(dict, course_name):
    with open("data/hoctot/" + str(course_name) + ".csv", 'a', encoding='utf-8') as f_object:
        # Pass the file object and a list
        # of column names to DictWriter()
        # You will get a object of DictWriter
        dictwriter_object = DictWriter(f_object, fieldnames=list(dict.keys()))

        # Pass the dictionary as an argument to the Writerow()
        dictwriter_object.writerow(dict)

        # Close the file object
        f_object.close()


def login(driver, exam_url):
    driver.get(exam_url)
    driver.find_element_by_css_selector(".login_").click()
    time.sleep(1)

    user_name = driver.find_element_by_id('account-input')
    user_name.send_keys('ftech')

    password = driver.find_element_by_id('password-input')
    password.send_keys('Abcd1234')

    driver.find_element_by_css_selector('.btn-login-v1').click()


def do_test(driver, number_of_question, course_name):
    try:
        play_button = driver.find_element_by_css_selector('.item_right > .button-play-main')
        driver.execute_script("window.scrollTo(0,250);")
    except:
        play_button = driver.find_element_by_css_selector('.button-play-main')
    play_button.click()
    time.sleep(2)

    driver.find_element_by_id('btn-submit-game').click()
    time.sleep(1)

    driver.find_element_by_css_selector('.btn-close-bottom[data-result="true"]').click()
    time.sleep(2)

    driver.execute_script("window.scrollTo(0,250);")
    driver.find_element_by_css_selector('.item_left > .button-play-main').click()
    time.sleep(2)

    questions = driver.find_elements_by_class_name('question-item-main-panel')
    if len(questions) < number_of_question and course_name != "tieng-anh":
        questions = driver.find_elements(By.XPATH, "//*[contains(@id, 'childQuestion')]")
    return questions


def get_question_content(driver, main_div):
    question_text = main_div.find_element_by_css_selector(".card-game-content").text
    question_text_list = question_text.split("\n")

    all_mathjax = main_div.find_element_by_css_selector(".card-game-content").find_elements(By.TAG_NAME, "script")
    mathjax_list = ["$ " + x.get_attribute("innerHTML") + " $" for x in all_mathjax]

    iters = [iter(question_text_list), iter(mathjax_list)]
    full_question = list(itertools.chain(map(next, itertools.cycle(iters)), *iters))
    full_question = " ".join(full_question)

    try:
        img = main_div.find_element_by_css_selector(".card-game-content").find_element(By.TAG_NAME, "img").get_attribute('src')
        # full_question = full_question + "\n" + img
    except:
        img = ""
    return full_question, img


def get_question_answers(main_div):
    all_answers = main_div.find_elements_by_css_selector(".ks-checkBox")
    answer_status = [x.get_attribute("className") for x in main_div.find_elements_by_css_selector(".ks-checkBox")]
    correct_answer_id = [answer_status.index(x) for x in answer_status if "correct" in x]

    answer_list = []
    for answer in all_answers:
        answer_content = answer.find_element_by_css_selector(".gwt-HTML")

        # answer_text_list = answer_content.text.split("\n")
        try:
            answer_img = answer_content.find_element(By.TAG_NAME, "img").get_attribute("src")
        except:
            answer_img = False
        if answer_img:
            answer_text_list = [answer_img]
        else:
            answer_text_list = answer_content.get_attribute("innerHTML").split("\n")

        answer_mathjax = answer_content.find_elements(By.TAG_NAME, "script")
        mathjax_list = ["$ " + x.get_attribute("innerHTML") + " $" for x in answer_mathjax]

        iters = [iter(answer_text_list), iter(mathjax_list)]
        full_answer = list(itertools.chain(map(next, itertools.cycle(iters)), *iters))
        full_answer = " ".join(full_answer)
        answer_list.append(full_answer)
    try:
        hint_answer = main_div.find_element_by_css_selector(".game-code-view-hint-answer")
        hint_answer_text_list = hint_answer.text.split("\n")
        # hint_answer_text_list = hint_answer.get_attribute("innerHTML").split("\n")

        hint_answer_text_list = [x for x in hint_answer_text_list if "Kỹ năng" not in x and x != "" and x != "." and x != ',']
        hint_answer_mathjax = hint_answer.find_elements(By.TAG_NAME, "script")
        hint_answer_mathjax_list = ["$ " + x.get_attribute("innerHTML") + " $" for x in hint_answer_mathjax]
        iters = [iter(hint_answer_text_list), iter(hint_answer_mathjax_list)]
        full_hint = list(itertools.chain(map(next, itertools.cycle(iters)), *iters))
        full_hint = " ".join(full_hint)
        try:
            hint_img = hint_answer.find_element(By.TAG_NAME, "img").get_attribute("src")
            full_hint = full_hint + "\n" + str(hint_img)
        except:
            pass
    except:
        full_hint = ""
    correct_answer_text = answer_list[correct_answer_id[0]]
    answer_text = "\n".join(answer_list)
    return correct_answer_id, answer_text, full_hint, correct_answer_text


def get_data_from_exam(exam, course_id, course_name):
    driver = webdriver.Chrome()
    exam_id = exam["id"]
    exam_url = exam_root_url + exam["pathName"] + "-" + str(exam_id)
    question_ids = exam["childrentIds"]
    exam_title = exam['name']
    number_of_question = exam['questionNumber']
    login(driver, exam_url)
    time.sleep(3)
    questions = do_test(driver, number_of_question, course_name)
    if len(question_ids) < number_of_question:
        question_ids = [1] * number_of_question

    for question_index, question in enumerate(questions):
        main_div = question.find_element_by_css_selector(".game-code-view-main-view-panel")
        try:
            if course_name == "tieng-anh":
                fill_blank_question = question.find_element_by_css_selector(".game-code-view-main-view-panel").find_element_by_css_selector(".card-game-content").text
            else:
                fill_blank_question = ""
        except:
            fill_blank_question = ""
        try:
            main_div_list = main_div.find_elements(By.XPATH, ".//div[contains(@id, 'childQuestion')]")
            if len(main_div_list) == 0:
                main_div_list = [main_div]
        except:
            main_div_list = [main_div]
        for main_div_item in main_div_list:
            question_content, img = get_question_content(driver, main_div_item)
            correct_answer_id, answer_text, full_hint, correct_answer_text = get_question_answers(main_div_item)

            question_info = {
                'question_id': question_ids[question_index],
                'fill_blank_question': fill_blank_question,
                'question': question_content,
                'img': img,
                'answers': answer_text,
                'question_number': question_ids[question_index],
                'correct_answer_text': correct_answer_text,
                'correct_answer_id': correct_answer_id,
                'subject': course_name,
                'explain': full_hint,
                'grade': "12",
                "test_set_id": course_id,
                "test_set_title": course_name,
                "test_id": exam_id,
                "test_title": exam_title
            }
            append_dict_as_row(question_info, course_name)
            a = 0

    driver.close()


def do_crawl(output_directory):
    cat_id = HOCTOT_CAT_ID
    hoctot = HoctotCrawler()
    courses = hoctot.get_course(cat_id)
    for course in courses:
        course_id = course["id"]
        course_name = course['friendlyUrl']

        my_file = Path(output_directory + "/" + str(course_name) + ".csv")
        if my_file.is_file():
            pass
        else:
            default_df = pd.DataFrame(columns=['question_id', 'fill_blank_question', 'question', "img", 'answers', 'question_number', 'correct_answer_text', 'correct_answer_id', 'subject', "explain", "grade", "test_set_id", "test_set_title", "test_id", "test_title"])
            default_df.to_csv(output_directory + "/" + str(course_name) + ".csv", index=False)

        exams = hoctot.get_exam_by_course_id(course_id)
        for exam in exams:

            df = pd.read_csv(output_directory + "/" + str(course_name) + ".csv")
            test_id = df["test_id"].drop_duplicates().to_list()

            if exam["id"] in test_id or exam["id"] == 6624249577996288:
                continue

            get_data_from_exam(exam, course_id, course_name)


def main(argv):
    output_directory = ""
    try:
        opts, args = getopt.getopt(argv, "d:", ["directory-"])
    except getopt.GetoptError:
        print('hoctot_crawler.py -d <directory>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-d", "--directory"):
            output_directory = arg

    print('Output directory is ', output_directory)

    Path(output_directory).mkdir(parents=True, exist_ok=True)

    do_crawl(output_directory)


if __name__ == '__main__':
    main(sys.argv[1:])
