import json
import os
from dotenv import load_dotenv
import pandas as pd
import sys
import getopt
from csv import DictWriter
from pathlib import Path
from bs4 import BeautifulSoup as BS
from onluyen_crawler import OnluyenCrawler
import cloudscraper
import logging

logging.basicConfig(filename='crawler_log.log', level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

load_dotenv()
USER = os.environ.get("ONLUYEN_USER")
PASSWORD = os.environ.get("ONLUYEN_PASSWORD")


def append_dict_as_row(dict, subject, grade, output_directory):
    with open(output_directory + "/test_" + str(subject) + "_" + str(grade) + ".csv", 'a', encoding='utf-8') as f_object:
        # Pass the file object and a list
        # of column names to DictWriter()
        # You will get a object of DictWriter
        dictwriter_object = DictWriter(f_object, fieldnames=list(dict.keys()))

        # Pass the dictionary as an argument to the Writerow()
        dictwriter_object.writerow(dict)

        # Close the file object
        f_object.close()


def get_image(raw_html):
    img = ""
    try:
        soup = BS(raw_html)
        for imgtag in soup.find_all('img'):
            img = imgtag['src']
    except:
        img = ""
    return img


def cleanhtml(raw_html):
    cleantext = BS(raw_html, "lxml").text
    return cleantext


class OnluyenCrawlerTest(OnluyenCrawler):
    def get_test_categories(self, grade: str, subject: str, index: int):
        url = "https://api-elb.onluyen.vn/api/category/data/{grade}/{subject}/{index}".format(grade=grade, subject=subject, index=index)
        response = self.response_from_get_request(url=url, func_name="get_tests")
        return response

    def get_test_category_level(self, category_id: str, index: int = 0):
        url = "https://api-elb.onluyen.vn/api/tests/category/level/{category_id}/{index}".format(category_id=category_id, index=index)
        response = self.response_from_get_request(url=url, func_name="get_test_category_level")
        return response

    def get_test_category_level_detail(self, category_id: str, index: int = 0):
        url = "https://api-elb.onluyen.vn/api/tests/category/detail/{category_id}/null/{index}".format(category_id=category_id, index=index)
        response = self.response_from_get_request(url=url, func_name="get_test_category_level_detail")
        return response

    def get_all_questions_in_test(self, test_id):
        url = "https://api-elb.onluyen.vn/api/tests/questions/{test_id}".format(test_id=test_id)
        response = self.response_from_get_request(url=url, func_name="get_all_questions_in_test")
        return response

    def get_test_result(self, test_id):
        url = "https://api-elb.onluyen.vn/api/tests/detail/{test_id}".format(test_id=test_id)
        response = self.response_from_get_request(url=url, func_name="get_test_result")
        return response

    def start_test(self, test_id):
        url = "https://api-elb.onluyen.vn/api/tests/start/{test_id}".format(test_id=test_id)
        response = self.response_from_get_request(url=url, func_name="start_test")
        return response

    def done_test(self, test_id):
        url = 'https://api-elb.onluyen.vn/api/tests/done/{test_id}'.format(test_id=test_id)
        response = self.response_from_get_request(url=url, func_name="start_test")
        return response

    def send_answer(self, test_id: str, question_id: str, answer_selected="default", skip=False):
        if answer_selected == "default":
            answer_selected = [0]
        send_answer_url = "https://api-elb.onluyen.vn/api/tests/sendanswer"
        answer = {
            'dataOptionId': answer_selected,
            'dataOptionText': [],
            'isSkip': skip,
            'problemId': "",
            'stepId': question_id,
            "testId": test_id,
            "textAnswer": "",
        }

        header = {'Authorization': 'Bearer {}'.format(self.access_token)}
        scraper = cloudscraper.create_scraper()

        response = scraper.post(
            send_answer_url,
            json=answer,
            headers=header
        )
        try:
            response_text = json.loads(response.text)
        except Exception as e:
            logging.debug(response)


def extract_question_info(test_result, set_of_test_title, test_title, subject, grade, output_directory):
    list_question = test_result["listQuestion"]
    for i, question in enumerate(list_question):
        question_info = {}
        try:
            if question["dataStandard"] is not None:
                question_data = question["dataStandard"]
            elif question['dataMaterial'] is not None:
                question_data = question['dataMaterial']
            else:
                question_data = None
                logging.debug("Question data is None")
            if question_data is not None:
                try:
                    if "data" not in question_data:
                        all_answers = "\n".join([cleanhtml(x["content"]) for x in question_data['options']])
                        if all_answers.strip() == "":
                            all_answers = "\n".join([get_image(x["content"]) for x in question_data['options']])

                        all_answers = "".join([s for s in all_answers.strip().splitlines(True) if s.strip()])
                        answer_option_id = question_data['answerOptionId']
                        if len(answer_option_id) > 0:
                            correct_answer_text = cleanhtml(question_data['options'][question_data['answerOptionId'][0]]["content"])
                            if correct_answer_text == "":
                                correct_answer_text = get_image(question_data['options'][question_data['answerOptionId'][0]]["content"])
                        else:
                            correct_answer_text = ""

                        question_info = {
                            'question_id': question_data["stepId"],
                            'fill_blank_question': "",
                            'question': cleanhtml(question_data["question"]),
                            'img': get_image(question_data["question"]),
                            'answers': all_answers,
                            'question_number': question_data['numberQuestion'],
                            'correct_answer_text': correct_answer_text,
                            'correct_answer_id': answer_option_id,
                            'subject': subject,
                            'explain': cleanhtml(question_data['explainQuestion']) if question_data['explainQuestion'] else "",
                            'grade': grade,
                            "test_set_id": test_result['testCategoryId'],
                            "test_set_title": set_of_test_title,
                            "test_id": question_data["testId"],
                            "test_title": test_title
                        }
                        append_dict_as_row(question_info, subject, grade, output_directory)
                    else:
                        fill_blank_question = cleanhtml(question_data['contentHtml'])
                        for sub_question_data in question_data["data"]:
                            answer_option_id = sub_question_data['answerOptionId']
                            if len(answer_option_id) > 0:
                                correct_answer_text = cleanhtml(sub_question_data['options'][sub_question_data['answerOptionId'][0]]["content"])
                            else:
                                correct_answer_text = ""
                            question_info = {
                                'question_id': sub_question_data["stepId"],
                                'fill_blank_question': fill_blank_question,
                                'question': cleanhtml(sub_question_data["question"]),
                                'img': get_image(sub_question_data["question"]),
                                'answers': "\n".join([cleanhtml(x["content"]) for x in sub_question_data['options']]),
                                'question_number': sub_question_data['numberQuestion'],
                                'correct_answer_text': correct_answer_text,
                                'correct_answer_id': answer_option_id,
                                'subject': subject,
                                'explain': cleanhtml(sub_question_data['explainQuestion']) if sub_question_data['explainQuestion'] else "",
                                'grade': grade,
                                "test_set_id": test_result['testCategoryId'],
                                "test_set_title": set_of_test_title,
                                "test_id": sub_question_data["testId"],
                                "test_title": test_title
                            }
                            append_dict_as_row(question_info, subject, grade, output_directory)
                        # pass
                except Exception as e:
                    logging.debug(e)

        except Exception as e:
            logging.debug(e)
            logging.debug("Question data is None")


def do_test(onluyen, test_id):
    onluyen.start_test(test_id)
    all_questions = onluyen.get_all_questions_in_test(test_id=test_id)
    for question in all_questions:
        question_id = question['stepId']
        onluyen.send_answer(test_id=test_id, question_id=question_id)
    onluyen.done_test(test_id)


def get_all_test_in_set(onluyen, set_of_test):
    index = 0
    all_tests = []

    while True:
        tests = onluyen.get_test_category_level_detail(category_id=set_of_test['testCategoryId'], index=index)
        if len(tests['data']) == 0:
            break
        all_tests += tests['data']
        index += 1

    return all_tests


def get_set_of_tests_id(onluyen, set_test_id_list: list, category):
    sub_categories = onluyen.get_test_category_level(category_id=category['testCategoryId'])
    for sub_category in sub_categories:
        if sub_category['typeViewGroupTest'] == 4:
            set_test_id_list = get_set_of_tests_id(onluyen, set_test_id_list, sub_category)
        elif sub_category['typeViewGroupTest'] == 0:
            set_test_id_list += [sub_category]
    return set_test_id_list


def get_all_test_categories(onluyen, grade, subject):
    index = 0
    all_test_categories = []
    while True:
        test_categories = onluyen.get_test_categories(grade, subject, index)
        if len(test_categories) == 0:
            break
        all_test_categories += test_categories
        index += 1
    return all_test_categories


def do_crawl(grade, subject, output_directory):
    onluyen = OnluyenCrawlerTest()
    onluyen.login()
    set_of_tests_id = []
    all_test_categories = get_all_test_categories(onluyen, grade, subject)

    ## Get set of tests from all categories
    for category in all_test_categories:
        if category['typeViewGroupTest'] == 4:
            set_of_tests_id = get_set_of_tests_id(onluyen, set_of_tests_id, category)
        elif category['typeViewGroupTest'] == 0:
            set_of_tests_id += [category]

    for set_of_test in set_of_tests_id:
        set_of_test_title = set_of_test['title']
        set_of_test_id = set_of_test['testCategoryId']
        all_tests_in_set = get_all_test_in_set(onluyen, set_of_test)
        for test in all_tests_in_set:
            test_id = test['testId']
            test_title = test['title']
            print("Test set: " + str(set_of_test_title))
            print("Test: " + str(test_title))
            do_test(onluyen, test_id=test_id)
            test_result = onluyen.get_test_result(test_id=test_id)
            extract_question_info(test_result, set_of_test_title, test_title, subject, grade, output_directory)


def main(argv):
    input_grade = ""
    input_subject = ""
    output_directory = ""
    try:
        opts, args = getopt.getopt(argv, "g:s:d:", ["grade=", "subject=", "directory-"])
    except getopt.GetoptError:
        print('onluyen_crawler_test.py -g <grade> -s <subject> -d <directory>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-g", "--grade"):
            input_grade = arg
        elif opt in ("-s", "--subject"):
            input_subject = arg
        elif opt in ("-d", "--directory"):
            output_directory = arg

    print('Grade is', input_grade)
    print('Subject is', input_subject)
    print('Output directory is ', output_directory)

    Path(output_directory).mkdir(parents=True, exist_ok=True)


    my_file = Path(output_directory + "/test_" + str(input_subject) + "_" + str(input_grade) + ".csv")
    if my_file.is_file():
        pass
    else:
        default_df = pd.DataFrame(columns=['question_id', 'fill_blank_question', 'question', "img", 'answers', 'question_number', 'correct_answer_text', 'correct_answer_id', 'subject', "explain", "grade", "test_set_id", "test_set_title", "test_id", "test_title"])
        default_df.to_csv(output_directory + "/test_" + str(input_subject) + "_" + str(input_grade) + ".csv", index=False)

    do_crawl(input_grade, input_subject, output_directory)


if __name__ == '__main__':
    main(sys.argv[1:])
