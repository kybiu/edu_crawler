import base64
import logging
import os

import requests
import pandas as pd
import json
import time
import hashlib
import re

from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup

from config.constant import API_ASSIGNMENT_URL
from helper.object_storage import ObjectStorage

FOLDER = "v0.2/vietjack"


class VietJackCrawler:
    def __init__(self, grades, subjects, grade_map, subject_map):
        self.grades = grades
        self.subjects = subjects
        self.grade_map = grade_map
        self.subject_map = subject_map
        self.url = "https://khoahoc.vietjack.com/trac-nghiem/"
        self.browser = None
        self.assignment_name = ''
        self.object_storage = None
        self.count = 0
        self.log_fg_classifier = []

    def create_folder_name(self):
        h = hashlib.sha256()
        h_str = (self.assignment_name + str(time.time())).encode('utf-8')
        h.update(h_str)
        return "{}".format(h.hexdigest())

    def img_classifier(self, content):
        data = {"img": content}
        r = requests.post(url="http://192.168.1.21:5000/classify-img", json=data)
        if r.status_code == 200:
            confidence = r.json().get('result').get('confidence')
            label = r.json().get('result').get('label')
            return confidence, label
        else:
            return 0, 'error api'

    def mathpix_ocr(self, img):
        try:
            file_path = img
            image_uri = "data:image/png;base64," + base64.b64encode(open(file_path, "rb").read()).decode()
            r = requests.post("https://api.mathpix.com/v3/text", data=json.dumps({'src': image_uri}),
                              headers={"app_id": os.getenv('mathpix_id', ''), "app_key": os.getenv('mathpix_key', ''),
                                       "Content-type": "application/json"})
            self.count += 1
            if self.count % 500 == 0:
                print("current image through mathpix: ", self.count)
            if r.status_code == 200:
                res = r.json()
                mathjax = res.get('text')
                return mathjax, res
            else:
                print(r, os.getenv('mathpix_id', ''), os.getenv('mathpix_key', ''))
                print("Exception in Mathpix API ", r.status_code)
                return None, "error mathpix api"
        except Exception as e:
            print("Exception in mathpix : ", str(e), img)
            return None, "error mathpix api"

    def process_img(self, img_link, classify=True):
        img = img_link.split("/")[-1]
        r = requests.get(img_link, stream=True)
        if r.status_code == 200:
            with open(img, "wb") as fd:
                fd.write(r.content)

            self.object_storage = ObjectStorage("{}/{}".format(FOLDER, self.create_folder_name()), img, img)
            self.object_storage.post_file()

            mathjax = None
            res_math = None
            if classify:
                img_encode = str(base64.b64encode(r.content, ), 'utf-8')
                confidence, label = self.img_classifier(img_encode)
                if (confidence > 0.7) & (label == "formula"):
                    mathjax, res_math = self.mathpix_ocr(img)

                to_log = [self.object_storage.file_name, confidence, label, mathjax, res_math]
                self.log_fg_classifier.append(to_log)
            else:
                mathjax, res_math = self.mathpix_ocr(img)

            if not mathjax:
                mathjax = None
            os.remove(img)
            return mathjax, res_math, self.object_storage.file_name

        else:
            print('Error save image')
            os.remove(img)
            return None, None, self.object_storage.file_name

    def start_webdriver(self):
        # setting config visual
        firefox_profile = FirefoxProfile()
        # Disable CSS
        firefox_profile.set_preference('permissions.default.stylesheet', 2)
        # Disable images
        firefox_profile.set_preference('permissions.default.image', 2)
        # Disable Flash
        firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')

        self.browser = webdriver.Firefox(firefox_profile=firefox_profile)
        self.login()

    def login(self):
        self.browser.get(self.url)
        time.sleep(1)
        self.browser.find_element_by_class_name('login-icon').click()
        self.browser.find_element_by_class_name('btn-login').click()
        self.browser.find_elements_by_name('email')[1].send_keys('nguyenlinhchi01655907777@gmail.com', Keys.ENTER)
        self.browser.find_elements_by_name('password')[1].send_keys('P@ssw0rd', Keys.ENTER)
        time.sleep(2)

    def get_link_assignment(self):
        link_crawl = {}
        for sub in self.subjects:
            print('*********')
            print('subject: ', sub)
            for gr in self.grades:
                print('grade: ', gr)
                self.browser.get(f'https://khoahoc.vietjack.com/trac-nghiem/{gr}/{sub}')
                page = BeautifulSoup(self.browser.find_element_by_class_name('list-exam').get_attribute('innerHTML'),
                                     "lxml")

                chapters = page.find_all('div', {'class': 'chapter-item'})

                data = {}
                for c in chapters:
                    chapter_title = c.find('a', {'class': 'url-root'}).text.strip()

                    list_lesson = c.find_all('ul', {'class': 'lesson-wrapper'})[1:]
                    list_title_lesson = c.find_all('a', {'class': 'url-root', 'data-toggle': 'collapse'})
                    list_title_lesson = [i.text.strip() for i in list_title_lesson]

                    bt = {}
                    for n, i in enumerate(list_lesson):
                        list_bt = i.find_all('li', {'class': 'exam-item'})
                        tmp = []
                        for j in list_bt:
                            tmp2 = j.find('a', {'class': 'url-root'})
                            tmp.append({'title': tmp2.text.strip(), 'link': tmp2.get('href')})

                        bt[list_title_lesson[n]] = tmp

                    data[chapter_title] = bt

                data_update = {}
                for chapter, c_content in data.items():
                    lesson = {}
                    for k, v in c_content.items():
                        tmp = []
                        for item in v:
                            try:
                                res = requests.get(item.get('link'))
                                p = BeautifulSoup(res.text, 'lxml')
                                split = p.find('ul', {'class': 'tab-exam'}).find_all('a')
                                tmp += [{'title': i.get('title'), 'link': i.get('href')} for i in split]
                            except:
                                print(item.get('link'))
                        lesson[k] = tmp

                    data_update[chapter] = lesson

                link_crawl[f'{gr}_{sub}'] = data_update

                if not os.path.exists('data/'):
                    os.mkdir('data/')
                with open(f'data/link_crawl_{gr}_{sub}.json', 'w') as f:
                    json.dump(data_update, f)

        return link_crawl

    def get_data(self):
        for subject in self.subjects:
            print(subject)
            for grade in self.grades:
                print(grade)
                self.log_fg_classifier = []

                with open(f'data/link_crawl_{grade}_{subject}.json', 'r') as f:
                    data_update = json.load(f)

                data_test = []

                for chapters, c_content in data_update.items():
                    for lesson, l_content in c_content.items():
                        set_title = ''
                        if (not chapters.strip().lower().startswith("chương")) & (
                        lesson.strip().lower().endswith('bài')):
                            set_title += chapters + '_' + lesson
                        if (subject == 'mon-Van') & (not chapters.strip().lower().startswith('đề thi')):
                            set_title = False

                        for assign in l_content:
                            try:
                                self.assignment_name = assign.get('title')
                                self.browser.get(assign.get('link') + '/thi')
                                page = BeautifulSoup(self.browser.page_source, "lxml")
                                main_body = page.find('div', {'class': 'main-body'})

                                # pre-process html
                                for u in main_body.find_all('u'):
                                    u.string = "<u>" + u.text + "</u>"

                                for ins in main_body.find_all('ins'):
                                    ins.string = "<u>" + ins.text + "</u>"

                                for st in main_body.find_all('span', style=re.compile("text-decoration: underline")):
                                    st.string = "<u>" + st.text + "</u>"

                                for e in main_body.find_all('math'):
                                    tag = page.new_tag('p')
                                    tag.string = ' ' + str(e) + ' '
                                    e.insert_after(tag)
                                    e.string = ''

                                for reason in main_body.find_all('div', {"class": "question-reason"}):
                                    for sol in reason.find_all("img"):
                                        sol_src = sol.get('src')
                                        if ('C:/Users' not in sol_src) & ('http' in sol_src):
                                            link_image = sol_src
                                            mathjax, res_math, saved_image = self.process_img(link_image,
                                                                                              classify=False)
                                            if mathjax:
                                                p_tag = page.new_tag('p')
                                                p_tag.string = ' ' + '<mathjax>' + mathjax + '<mathjax>' + ' ' + "<origin>" + saved_image + "<origin>" + ' '
                                                sol.insert_after(p_tag)
                                                sol.unwrap()
                                            else:
                                                p_tag = page.new_tag('p')
                                                p_tag.string = ' ' + saved_image + ' '
                                                sol.insert_after(p_tag)
                                                sol.unwrap()
                                        else:
                                            p_tag = page.new_tag('p')
                                            p_tag.string = ' ' + sol_src + ' '
                                            sol.insert_after(p_tag)
                                            sol.unwrap()

                                for img in main_body.find_all('img'):
                                    img_src = img.get('src')
                                    if ('C:/Users' not in img_src) & ('http' in img_src):
                                        link_image = img_src
                                        if subject == 'mon-tieng-anh-moi':
                                            mathjax, res_math, saved_image = self.process_img(link_image,
                                                                                              classify=False)
                                        else:
                                            mathjax, res_math, saved_image = self.process_img(link_image)
                                        if mathjax:
                                            p_tag = page.new_tag('p')
                                            p_tag.string = ' ' + '<mathjax>' + mathjax + '<mathjax>' + ' ' + "<origin>" + saved_image + "<origin>" + ' '
                                            img.insert_after(p_tag)
                                            img.unwrap()
                                        else:
                                            p_tag = page.new_tag('p')
                                            p_tag.string = ' ' + saved_image + ' '
                                            img.insert_after(p_tag)
                                            img.unwrap()
                                    else:
                                        p_tag = page.new_tag('p')
                                        p_tag.string = ' ' + img_src + ' '
                                        img.insert_after(p_tag)
                                        img.unwrap()

                                # get info
                                meta_data = []
                                list_answer = main_body.find_all('div', {'class': 'quiz-answer-item'})
                                for ans in list_answer:
                                    try:
                                        name = ans.find('div', {'class': 'question-header'}).text.strip()

                                        text_content_block = ans.find('div', {'class': 'question-name'})
                                        raw_text_content = text_content_block.text.strip()
                                        tmp_text_content = re.findall(r".*<origin>(.*)<origin>.*", raw_text_content)
                                        text_content = raw_text_content
                                        for tc in tmp_text_content:
                                            text_content = text_content.replace(f"<origin>{tc}<origin>", "")
                                        text_content = text_content.replace("<mathjax>", "")

                                        raw_text_answer = ans.find_all('div', {'class': 'anwser-item'})
                                        raw_correct_answer = ''
                                        for a in raw_text_answer:
                                            if a.find('input').get('value') == 'Y':
                                                raw_correct_answer = a.text.strip()
                                                break
                                        correct_answer = raw_correct_answer
                                        tmp_correct_answer = re.findall(r".*<origin>(.*)<origin>.*",
                                                                        raw_correct_answer)
                                        for ca in tmp_correct_answer:
                                            correct_answer = correct_answer.replace(f"<origin>{ca}<origin>", "")
                                        correct_answer = correct_answer.replace("<mathjax>", "")

                                        raw_text_answer = '\n'.join([i.text.strip() for i in raw_text_answer])
                                        text_answer = raw_text_answer
                                        tmp_text_answer = re.findall(r".*<origin>(.*)<origin>.*",
                                                                     raw_text_answer)
                                        for ta in tmp_text_answer:
                                            text_answer = text_answer.replace(f"<origin>{ta}<origin>", "")
                                        text_answer = text_answer.replace("<mathjax>", "")

                                        raw_solution = ans.find('div', {'class': 'question-reason'}).text.replace(
                                            '\n\n', '').strip()
                                        solution = raw_solution
                                        tmp_solution = re.findall(r".*<origin>(.*)<origin>.*",
                                                                  raw_solution)
                                        for s in tmp_solution:
                                            solution = solution.replace(f"<origin>{s}<origin>", "")
                                        solution = solution.replace("<mathjax>", "")

                                        raw_question = raw_text_content + ';' + raw_text_answer + ';' + raw_correct_answer + ';' + raw_solution

                                        question_type = ''

                                        source_crawler = assign.get('link')

                                        source_detail = ''

                                        meta_data.append({
                                            "name": name,
                                            "raw_question": raw_question,
                                            "text_content": text_content,
                                            "text_answer": text_answer,
                                            "correct_answer": correct_answer,
                                            "solution": solution,
                                            "source_crawler": source_crawler,
                                            "question_type": question_type,
                                        })

                                        info = [grade, subject, chapters, lesson, name, raw_question, text_content,
                                                text_answer, correct_answer, solution, question_type, source_crawler,
                                                source_detail]
                                        data_test.append(info)
                                    except Exception as e:
                                        print('Exception question in : ', str(e), grade, subject, chapters, lesson,
                                              self.assignment_name)

                                data = {
                                    "grade_level": self.grade_map.get(grade),
                                    "subject": self.subject_map.get(subject),
                                    "chapter": chapters,
                                    "lesson": lesson,
                                    "title": self.assignment_name,
                                    "source_crawler": assign.get('link'),
                                    "storage_id": self.create_folder_name(),
                                    "meta_data": meta_data,
                                }
                                if set_title:
                                    data.update({"chapter": None})
                                    data.update({"lesson": None})
                                    data.update({"set_title": set_title})

                                # add in db
                                res = requests.post(API_ASSIGNMENT_URL, json=data)

                            except Exception as e:
                                print('Error in: ', str(e), grade, subject, chapters, lesson, self.assignment_name)

                df = pd.DataFrame(data_test)
                if len(data_test) > 0:
                    df.columns = ['grade_level', 'subject', 'chapter', 'lesson', 'name', 'raw_question', 'text_content',
                                  'text_answer', 'correct_answer', 'solution', 'question_type', 'source_crawler',
                                  'source_detail']
                    if not os.path.exists('data/vietjack/'):
                        os.mkdir('data/vietjack/')
                    df.to_csv(f'data/vietjack/data_{grade}_{subject}.csv', index=False)
                else:
                    print(f'not data in subject {subject}, {grade}')
                if self.log_fg_classifier:
                    df_log = pd.DataFrame(self.log_fg_classifier)
                    df_log.columns = ['file_name', 'confidence', 'label', 'latex', 'full_response']
                    df_log.to_csv(f'data/vietjack/fg_classifier_{grade}_{subject}.csv', index=False)

                print("number image convert: ", self.count)


if __name__ == '__main__':
    subjects = ['mon-Toan', 'mon-Vat-ly', 'mon-tieng-anh-moi', 'mon-Hoa-hoc', 'mon-Van', 'mon-sinh-hoc', 'mon-Lich-su',
                'mon-Dia-ly', 'mon-giao-duc-cong-dan']
    grades = ['Lop-12', 'Lop-11', 'Lop-10']

    subject_map = {'mon-Toan': 'Math', 'mon-Vat-ly': 'Physics', 'mon-Hoa-hoc': 'Chemistry',
                   'mon-tieng-anh-moi': 'English', 'mon-Van': 'Literature', 'mon-sinh-hoc': 'Biology',
                   'mon-Lich-su': 'History', 'mon-Dia-ly': 'Geography', 'mon-giao-duc-cong-dan': 'GDCD'}
    grade_map = {'Lop-12': '12', 'Lop-11': '11', 'Lop-10': '10'}

    crawler = VietJackCrawler(grades, subjects, grade_map, subject_map)
    crawler.start_webdriver()
    # crawler.get_link_assignment()
    if not os.path.exists('data/'):
        print('get link ...')
        crawler.get_link_assignment()
    crawler.get_data()
    print('Success')
