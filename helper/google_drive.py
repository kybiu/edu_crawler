from __future__ import print_function
import io
import os
import pickle
import re
from pathlib import Path

import requests
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.http import MediaIoBaseDownload

SCOPES = ['https://www.googleapis.com/auth/drive']
BASE_DIR = Path(__file__).resolve().parent.parent


def download_file(file_id, file_name):
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('{}/{}'.format(BASE_DIR, 'config/token.pickle')):
        with open('{}/{}'.format(BASE_DIR, 'config/token.pickle'), 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                '{}/{}'.format(BASE_DIR, 'config/token.pickle'), SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('{}/{}'.format(BASE_DIR, 'config/token.pickle'), 'wb') as token:
            pickle.dump(creds, token)

    drive_service = build('drive', 'v3', credentials=creds)
    request = drive_service.files().get_media(fileId=file_id)
    file_type = drive_service.files().get(fileId=file_id).execute()['name'].split('.')[-1]

    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
        print("Download %d%%." % int(status.progress() * 100))
    fh.seek(0)
    with open('{}/{}'.format(BASE_DIR, '{}.{}'.format(file_name, file_type)), "wb") as fd:
        fd.write(fh.read())
        fd.close()
    return '{}.{}'.format(file_name, file_type)

