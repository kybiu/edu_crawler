from mathpix.mathpix import MathPix

APP_ID = "purchase_ftech_ai_61fa32_a6da50"
APP_KEY = "776b9a3640450fa69c36"


def covert_image_to_latex(url):
    mathpix = MathPix(app_id=APP_ID,
                      app_key=APP_KEY)

    ocr = mathpix.process_image(
        image_url=url)
    return ocr.latex
