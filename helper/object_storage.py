import time
import hashlib
from pathlib import Path
from minio import Minio

from config.constant import ENDPOINT, ACCESS_KEY, SECRET_KEY, BOCKET_NAME

BASE_DIR = Path(__file__).resolve().parent.parent


class ObjectStorage:
    def __init__(self, folder, original_name=None, file_dir=None):
        self.bucket_name = BOCKET_NAME
        self.folder = folder
        self.original_name = original_name
        self.file_dir = file_dir
        if self.original_name:
            self.file_name = self.create_file_name()
        self.minio_client = Minio(endpoint=ENDPOINT, access_key=ACCESS_KEY,
                                  secret_key=SECRET_KEY, secure=True)

    def create_file_name(self):
        file_type = self.file_dir.split(".")[-1]
        h = hashlib.sha256()
        h_str = (self.original_name + str(time.time())).encode('utf-8')
        h.update(h_str)
        return "{}.{}".format(h.hexdigest(), file_type)

    def init_bucket(self):
        found = self.minio_client.bucket_exists(self.bucket_name)
        if found:
            print("exists.")
        else:
            self.minio_client.make_bucket(self.bucket_name)

    def post_file(self):
        self.minio_client.fput_object(self.bucket_name, "{}/{}".format(self.folder, self.file_name), self.file_dir)

    def get_file(self, file_name, saved_file_name):
        self.minio_client.fget_object(self.bucket_name,
                                      "{}/{}".format(self.folder, file_name),
                                      str(BASE_DIR) + saved_file_name)
