import json
import os
from dotenv import load_dotenv
import pandas as pd
import sys
import getopt
from csv import DictWriter
from pathlib import Path
from bs4 import BeautifulSoup as BS
import cloudscraper
import logging

logging.basicConfig(filename='crawler_log.log', level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

load_dotenv()
USER = os.environ.get("ONLUYEN_USER")
PASSWORD = os.environ.get("ONLUYEN_PASSWORD")
grades = ['C10', 'C11', 'C12']
subjects = ['TOAN', 'VATLY', 'HOAHOC', 'ANHTHPT2', 'TIENGANHTHPT3']
subjects_2 = ['NGUVAN', 'SINH', 'LICHSU', 'DIALY', 'GDCD']

output_data = []
answer_sequence = ["skip", "skip", "skip", "correct"]


def append_dict_as_row(dict, subject, grade):
    with open("data/practice/" + str(subject) + "_" + str(grade) + ".csv", 'a', encoding='utf-8') as f_object:
        # Pass the file object and a list
        # of column names to DictWriter()
        # You will get a object of DictWriter
        dictwriter_object = DictWriter(f_object, fieldnames=list(dict.keys()))

        # Pass the dictionary as an argument to the Writerow()
        dictwriter_object.writerow(dict)

        # Close the file object
        f_object.close()


def get_image(raw_html):
    img = ""
    try:
        soup = BS(raw_html)
        for imgtag in soup.find_all('img'):
            img = imgtag['src']
    except:
        img = ""
    return img


def cleanhtml(raw_html):
    if "<ins>" in raw_html or "<u>" in raw_html:
        raw_html = raw_html.replace("<ins>", "#").replace("</ins>", "@")
        raw_html = raw_html.replace("<u>", "#").replace("</u>", "@")

    cleantext = BS(raw_html, "lxml").text

    if "#" in cleantext and "@" in cleantext:
        cleantext = cleantext.replace("#", "<u>").replace("@", "</u>")
    return cleantext


class OnluyenCrawler:
    def __init__(self):
        self.access_token = None

    def login(self):
        logging.debug("Login")
        url = 'https://oauth.onluyen.vn/api/account/login'
        # url = 'https://oauth.onluyen.vn/api/account/login/social'
        login_info = {
            'userName': USER,
            'password': PASSWORD,
            # 'phoneNumber': "0935991985",
            # 'rememberMe': True,
            # 'socialType': "Email"
        }
        # login_info = {
        #     "socialId": "103095541255463778255",
        #     "socialToken": "eyJhbGciOiJSUzI1NiIsImtpZCI6IjAzYjJkMjJjMmZlY2Y4NzNlZDE5ZTViOGNmNzA0YWZiN2UyZWQ0YmUiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiODI2NDY4MTcxMzM3LWY0YXQ2aXNoODM1bGNpcWRzNWxzMWtjN3BlNjFwc2NyLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiODI2NDY4MTcxMzM3LWY0YXQ2aXNoODM1bGNpcWRzNWxzMWtjN3BlNjFwc2NyLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTAzMDk1NTQxMjU1NDYzNzc4MjU1IiwiaGQiOiJmdGVjaC5haSIsImVtYWlsIjoiYW5obmQyQGZ0ZWNoLmFpIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF0X2hhc2giOiJRaUZjS01LME5ibXByU0JPMXJYV2RBIiwibmFtZSI6IkFuaCBOZ3V54buFbiDEkOG7qWMiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDQuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1OTi1WMFI0VVpudy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BTVp1dWNuaXNvOVR2QnVTYVkxSGJ6MFY1aVctOEt5RkFRL3M5Ni1jL3Bob3RvLmpwZyIsImdpdmVuX25hbWUiOiJBbmgiLCJmYW1pbHlfbmFtZSI6Ik5ndXnhu4VuIMSQ4bupYyIsImxvY2FsZSI6ImVuIiwiaWF0IjoxNjEyNTE5MTk2LCJleHAiOjE2MTI1MjI3OTYsImp0aSI6Ijg5OGE4NmIwZDJlZDU0NDA4NjEwZjhmZjVhMmZhZDJiYjdkNTc0M2YifQ.N6FS4m_LoLUUG46Ir3pWBmV-8uhJ7jzlqjRHI8NWIg12FzwFnSVyrGaELZ19w6g4D9ej_rDFBi0OLJP1oXCexQ3xIckQec9re0dw0ijq7XwDsloe97ehh-t-vQYBY8TLS99KAOsDbjTpJN53Qgr4mSzCKXGQCHyptXHvL_eXzIkVo3nZ0t9rChYE2DUs_9kZwPm_MabucCznHmrJu9cFtu8v1Nom7zJwC-dtjUghIhgBf13RSfxaIH3vMc3dDFL163bdy7NJZRlt6b0tae8wYHJ3jkFNo-wxoIfBuOzyxLTP4u_LxjoSh6nbhoUG1Twl0_-Ee9UEQr8GGXSrB1kMSA",
        #     "socialType": "GOOGLE"
        # }
        headers = {'Content-type': 'application/json',
                   'Accept': 'application/json',
                   'User-Agent': "Chrome"}

        scraper = cloudscraper.create_scraper()
        response = scraper.post(
            url,
            json=login_info,
            headers=headers,
        )

        try:
            response = json.loads(response.text)
            self.access_token = response["access_token"]
        except Exception as e:
            logging.debug('Error {} when login onluyen'.format(str(e)))

    def response_from_get_request(self, url: str, func_name: str):
        if self.access_token:
            retry_time = 0
            response = None
            header = {'Authorization': 'Bearer {}'.format(self.access_token)}
            status_code = 0
            while response is None:
                scraper = cloudscraper.create_scraper()

                r = scraper.get(url, headers=header)
                status_code = r.status_code
                try:
                    response = json.loads(r.text)
                except Exception as e:
                    retry_time += 1
                    logging.debug(url)
                    logging.debug(r)
                    logging.debug("RETRYTIMES: " + str(retry_time))
                    logging.debug('Error {} in {func_name} with url : {url}'.format(str(e), func_name=func_name, url=url))
                    response = None

                if response is not None or retry_time > 3:
                    break

            return response
        else:
            logging.debug("No access token")
            return None

    def get_subject_by_grade(self, grade: str):
        """
        Grade params example: C12 for grade 12;  C11 for grade 11, ...
        :param grade:
        """
        url = "https://api-elb.onluyen.vn/api/subject/{grade}"
        url = url.format(grade=grade)
        response = self.response_from_get_request(url=url, func_name="get_subject_by_grade")
        return response

    def get_chapters(self, grade: str, subject: str):
        """
        url sample: https://api-elb.onluyen.vn/api/problemHierachy/C11/TOAN
        :param grade:
        :param subject:
        """
        logging.debug("Get chapter")

        url = "https://api-elb.onluyen.vn/api/problemHierachy/{grade}/{subject}".format(grade=grade, subject=subject)
        response = self.response_from_get_request(url=url, func_name="get_chapters")
        return response

    def get_lessons(self, chapter: dict):
        logging.debug("Get lesson")

        chapter_id = chapter["id"]
        url = "https://api-elb.onluyen.vn/api/problemHierachy/listProblem/{chapter_id}".format(chapter_id=chapter_id)
        response = self.response_from_get_request(url=url, func_name="get_lessons")
        return response

    def get_lesson_first_question(self, lesson: dict):
        first_question = {}
        lesson_id = lesson["id"]
        lesson_info_url = "https://api-elb.onluyen.vn/api/practice/start/{lesson_id}".format(lesson_id=lesson_id)
        # lesson_info_url = "https://api-elb.onluyen.vn/api/practice/info/{lesson_id}".format(lesson_id=lesson_id)
        try:

            response = self.response_from_get_request(url=lesson_info_url, func_name="get_lessons")
            first_question = {
                "l_id": lesson_id,
                "q_id": response["stepIdNow"],
            }
        except Exception as e:
            logging.debug('Error {} in {func_name} with url : {url}'.format(str(e), func_name="get_lesson_first_question",
                                                                            url=lesson_info_url))
            return None
        return first_question

    def get_question_content(self, lesson_id: str, question_id: str, old_question_id="0", data_material_index=0):
        logging.debug("Get question content")

        question_content = {}
        question_url = "https://api-elb.onluyen.vn/api/practice/questions/detail/{lesson_id}/{question_id}".format(
            lesson_id=lesson_id, question_id=question_id)
        response = self.response_from_get_request(url=question_url, func_name="get_question_content")

        try:
            if response["dataStandard"]:
                question_data = response["dataStandard"]

                all_answers = "\n".join([cleanhtml(x["name"]) for x in question_data['options']])
                if all_answers.strip() == "":
                    all_answers = "\n".join([get_image(x["name"]) for x in question_data['options']])

                all_answers = "".join([s for s in all_answers.strip().splitlines(True) if s.strip()])

                question_correct_answer = [x for x in question_data['options'] if x["isAnswer"]]
                if len(question_correct_answer) > 0:
                    correct_answer_text = "\n".join([cleanhtml(x["name"]) for x in question_data['options'] if x["isAnswer"]])
                    if correct_answer_text.strip() == "":
                        correct_answer_text = "\n".join([get_image(x["name"]) for x in question_data['options'] if x["isAnswer"]])
                else:
                    correct_answer_text = ""

                question_correct_answer_id = [x["id"] for x in question_data["options"] if x["isAnswer"]]
                question_incorrect_answer_id = [x["id"] for x in question_data["options"] if not x["isAnswer"]][:1]
                question_number = question_data['questionNumber']
                subject = question_data['subjectId']
                lesson_name = question_data['problemName']

                question_content = {
                    # 'id': question_id,
                    'question_id': question_data["id"],
                    'fill_blank_question': "",
                    'question': cleanhtml(question_data["question"]),
                    'img': get_image(question_data["question"]),
                    'answers': all_answers,
                    'question_number': question_number,
                    'correct_answer_text': correct_answer_text,
                    'correct_answer_id': question_correct_answer_id,
                    'incorrect_answer_id': question_incorrect_answer_id,
                    'subject': subject,
                    'lesson': lesson_name
                }
                if question_data["id"] is None:
                    logging.debug("Question id is None")
                    logging.debug(question_url)
                    return None, None
                return [question_content], data_material_index

            elif response["dataMaterial"]:
                if old_question_id == question_id:
                    data_material_index += 1
                else:
                    data_material_index = 0
                question_data = response["dataMaterial"]["listStep"]
                question_content = []
                fill_blank_question = cleanhtml(response["dataMaterial"]['contentHtml'])
                for question_data_item in question_data:

                    all_answers = "\n".join([cleanhtml(x["name"]) for x in question_data_item['options']])
                    if all_answers.strip() == "":
                        all_answers = "\n".join([get_image(x["name"]) for x in question_data_item['options']])

                    all_answers = "".join([s for s in all_answers.strip().splitlines(True) if s.strip()])

                    question_correct_answer = [x for x in question_data_item['options'] if x["isAnswer"]]
                    if len(question_correct_answer) > 0:
                        correct_answer_text = "\n".join([cleanhtml(x["name"]) for x in question_data_item['options'] if x["isAnswer"]])
                        if correct_answer_text.strip() == "":
                            correct_answer_text = "\n".join([get_image(x["name"]) for x in question_data_item['options'] if x["isAnswer"]])
                    else:
                        correct_answer_text = ""

                    question_correct_answer_id = [x["id"] for x in question_data_item["options"] if x["isAnswer"]]
                    question_incorrect_answer_id = [x["id"] for x in question_data_item["options"] if not x["isAnswer"]][:1]
                    question_number = question_data_item['questionNumber']
                    subject = question_data_item['subjectId']
                    lesson_name = question_data_item['problemName']

                    sub_question_content = {
                        # 'id': question_id,
                        'question_id': question_data_item["id"],
                        'fill_blank_question': fill_blank_question,
                        'question': cleanhtml(question_data_item["question"]),
                        'img': get_image(question_data_item["question"]),
                        'answers': all_answers,
                        'question_number': question_number,
                        'correct_answer_text': correct_answer_text,
                        'correct_answer_id': question_correct_answer_id,
                        'incorrect_answer_id': question_incorrect_answer_id,
                        'subject': subject,
                        'lesson': lesson_name
                    }
                    question_content.append(sub_question_content)
                return question_content, data_material_index
            else:
                return None, None
        except Exception as e:
            logging.debug(response)
            logging.debug(question_url)
            return None, None

    def send_answer(self, lesson_id: str, question_id: str, correct_answer_id=None, skip=True):
        logging.debug("Send answer")

        if correct_answer_id is None:
            correct_answer_id = []
        send_answer_url = "https://api-elb.onluyen.vn/api/practice/questions/sendanswer"
        answer = {
            'dataOptionId': correct_answer_id,
            'dataOptionText': [],
            'isSkip': skip,
            'problemId': lesson_id,
            'stepId': question_id,
            "testId": "",
            "textAnswer": "",

        }
        header = {'Authorization': 'Bearer {}'.format(self.access_token)}
        scraper = cloudscraper.create_scraper()

        response = scraper.post(
            send_answer_url,
            json=answer,
            headers=header
        )

        try:
            response = json.loads(response.text)
            next_question_id = response["nextStepId"]
            if response['explain']:
                explain = cleanhtml(response['explain'])
            else:
                explain = ""
            percent_complete = response["percentComplete"]
            total_question = response['totalQuestion']
            try:
                answer_option_id = response["answerOptionId"]
            except:
                answer_option_id = []
            correct_options = [x["content"] for x in response["options"] if x["isAnswer"]]
            if len(correct_options) > 0:
                correct_option_text = "\n".join([cleanhtml(x) for x in correct_options])
                if correct_option_text.strip() == "":
                    correct_option_text = "\n".join([get_image(x) for x in correct_options])
            else:
                correct_option_text = ""
            correct_option_text = correct_option_text.strip()
            answer_response = {
                "answer_explain": explain,
                "next_question_id": next_question_id,
                "percent_complete": percent_complete,
                "total_question": total_question,
                "answer_option_id": answer_option_id,
                "answer_option_content": correct_option_text
            }
            return answer_response
        except Exception as e:
            logging.debug(send_answer_url)
            logging.debug('Error {} in send answer'.format(str(e)))
            return None


def reset_lesson(onluyen: OnluyenCrawler, lesson, retry_time=0):
    """
    Keep sending incorrect answer until percent complete = 0%
    :rtype: object
    """
    logging.debug("send answer reset")
    first_question = onluyen.get_lesson_first_question(lesson=lesson)
    question_id = first_question["q_id"]
    lesson_id = lesson["id"]
    if question_id is None and retry_time < 5:
        retry_time += 1
        reset_lesson(onluyen, lesson, retry_time)
    is_break = False
    while True:
        # if question_id is None:
        #     a = 0
        question_content, data_material_index = onluyen.get_question_content(lesson_id=lesson_id,
                                                                             question_id=question_id)
        if question_content is None:
            break
        for question_content_item in question_content:
            incorrect_answer = question_content_item['incorrect_answer_id']
            answer_response = onluyen.send_answer(lesson_id=lesson_id, question_id=question_id,
                                                  correct_answer_id=incorrect_answer, skip=False)
            if answer_response is None or answer_response["percent_complete"] == 0:
                is_break = True
                break
            question_id = answer_response["next_question_id"]
        if is_break:
            break
    return


def do_crawl(grade: str, subject: str):
    file_df = pd.read_csv("data/practice/" + str(subject) + "_" + str(grade) + ".csv")
    onluyen = OnluyenCrawler()

    # Login onluyen to get access_token
    onluyen.login()

    ## Get all chapters of specific subject
    chapters = onluyen.get_chapters(grade=grade, subject=subject)
    for chapter in chapters:
        ## Get all lessons of specific chapter
        lessons = onluyen.get_lessons(chapter=chapter)
        list_problem = lessons['listProblem']

        for lesson in list_problem:

            crawl_times = 0
            all_question_number = file_df["question_number"].drop_duplicates().to_list()
            all_question_id = file_df["question_id"].drop_duplicates().to_list()

            ## Reset question level to 0%
            lesson_percent_complete = lesson["percentComplete"]
            if lesson_percent_complete != 0:
                reset_lesson(onluyen, lesson)

            ## Each lesson crawl 5 times
            while crawl_times < 5:
                crawl_times += 1
                duplicated_question_times = 0
                answer_sequence_index = 0
                skip_duplicate_times = 0
                question_number = ...

                ## Get the first question in a lesson
                first_question = onluyen.get_lesson_first_question(lesson=lesson)

                question_id = first_question["q_id"]
                lesson_id = lesson["id"]
                data_material_index = 0
                old_question_id = "0"
                question_number_repeat_time = 0
                while True:
                    if question_number_repeat_time > 6:
                        break
                    answer_type = answer_sequence[answer_sequence_index]

                    ## If not duplicate question id

                    # if question_number not in all_question_number:
                    if question_id not in all_question_id:
                        ## Get question content
                        question_content = {}

                        question_content, data_material_index = onluyen.get_question_content(lesson_id=lesson_id,
                                                                                             question_id=question_id,
                                                                                             old_question_id=old_question_id,
                                                                                             data_material_index=data_material_index)
                        if question_content is None:
                            break
                        for question_content_item in question_content:

                            question_number = question_content_item["question_number"]
                            old_question_id = question_id
                            question_id = question_content_item["question_id"]

                            ## Send answer: each level send 1 correct 3 skip
                            answer_to_send = ...
                            is_skip = ...

                            if answer_type == "correct":
                                answer_to_send = question_content_item["correct_answer_id"]
                                is_skip = False
                                skip_duplicate_times = 0
                            elif answer_type == "skip":
                                answer_to_send = None
                                is_skip = True

                            answer_response = onluyen.send_answer(lesson_id=lesson_id, question_id=question_id,
                                                                  correct_answer_id=answer_to_send, skip=is_skip)
                            if answer_response is None:
                                break
                            question_content_item["explain"] = answer_response["answer_explain"]
                            question_content_item["chapter"] = chapter['name']
                            question_content_item["grade"] = grade
                            question_content_item["level"] = answer_response["percent_complete"]
                            if len(question_content_item["correct_answer_id"]) == 0:
                                question_content_item["correct_answer_id"] = answer_response['answer_option_id']
                            if question_content_item["correct_answer_text"] == "":
                                question_content_item["correct_answer_text"] = answer_response["answer_option_content"]

                            ## IF not duplicate question number add question to list question_without_answer
                            if question_number not in all_question_number:
                                # if question_number not in all_question_number and question_number in question_without_answer:
                                all_question_number.append(question_number)
                                all_question_id.append(question_id)
                                # output_data.append(question_content)
                                append_dict_as_row(question_content_item, subject, grade)
                            else:
                                question_number_repeat_time += 1
                    else:
                        logging.debug("Send answer skip")
                        ## if skip duplicated time exceed 15 then send correct  answer to get to next level
                        if skip_duplicate_times > 20:
                            skip_duplicate_times = 0
                            question_content, data_material_index = onluyen.get_question_content(lesson_id=lesson_id,
                                                                                                 question_id=question_id)
                            if question_content is None:
                                break
                            for question_content_item in question_content:
                                answer_to_send = question_content_item["correct_answer_id"]
                                is_skip = False
                                answer_response = onluyen.send_answer(lesson_id=lesson_id, question_id=question_id,
                                                                      correct_answer_id=answer_to_send, skip=is_skip)

                            if answer_response is None:
                                break
                        else:
                            ## If question already crawled skip the question
                            answer_response = onluyen.send_answer(lesson_id=lesson_id, question_id=question_id,
                                                                  correct_answer_id=None, skip=True)
                            if answer_response is None:
                                break
                            skip_duplicate_times += 1

                    if answer_response["next_question_id"] is None:
                        break
                    question_id = answer_response["next_question_id"]

                    ## Increase answer seq index each loop, if exceed 3 reset to 0
                    answer_sequence_index += 1
                    if answer_sequence_index > 3:
                        answer_sequence_index = 0


def main(argv):
    input_grade = ""
    input_subject = ""
    output_directory = ""
    try:
        opts, args = getopt.getopt(argv, "g:s:d:", ["grade=", "subject=", "directory-"])
    except getopt.GetoptError:
        print('onluyen_crawler.py -g <grade> -s <subject> -d <directory>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-g", "--grade"):
            input_grade = arg
        elif opt in ("-s", "--subject"):
            input_subject = arg
        elif opt in ("-d", "--directory"):
            output_directory = arg

    print('Grade is', input_grade)
    print('Subject is', input_subject)
    print('Output directory is ', output_directory)
    Path(output_directory).mkdir(parents=True, exist_ok=True)

    my_file = Path(output_directory + "/" + str(input_subject) + "_" + str(input_grade) + ".csv")
    if my_file.is_file():
        pass
    else:
        default_df = pd.DataFrame(
            columns=['question_id', "fill_blank_question", 'question', "img", 'answers', 'question_number', 'correct_answer_text', 'correct_answer_id',
                     'incorrect_answer_id', 'subject', 'lesson', "explain", "chapter", "grade", "level"])
        default_df.to_csv(output_directory + "/" + str(input_subject) + "_" + str(input_grade) + ".csv", index=False)

    do_crawl(input_grade, input_subject)


if __name__ == '__main__':
    main(sys.argv[1:])
