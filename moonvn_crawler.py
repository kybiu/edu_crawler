import os
import random

import requests
from pathlib import Path
from bs4 import BeautifulSoup

from helper.object_storage import ObjectStorage
from config.constant import API_ASSIGNMENT_URL

URL = "https://moon.vn/de-thi/de-thi-thu-thpt-quoc-gia-2019-mon-sinh-hoc--cum-7-truong-chuyen-thpt-86444"
BASE_DIR = Path(__file__).resolve().parent.parent
SOURCE = "https://moon.vn"
FOLDER = "moon-vn"


class MoonCrawler:
    def __init__(self, url):
        self.source_crawler = url
        self.grade, self.subject, self.name, self.link_file = self.get_info()
        self.saved_name = None
        self.obj_storage = None
        self.file = None

    def get_info(self):
        r = requests.get(self.source_crawler)
        soup = BeautifulSoup(r.text, "lxml")
        name = soup.find("title").text.strip()
        name = " ".join(name.split())
        grade, subject = 12, "English"

        # get grade
        if "thpt quốc gia" in name.lower() or "lớp 12" in name.lower():
            grade = 12
        if "lớp 11" in name.lower():
            grade = 11
        if "lớp 10" in name.lower():
            grade = 10

        # get subject
        if "vật lý" in name.lower() or "vật lí" in name.lower():
            subject = "Physics"
        if "toán" in name.lower():
            subject = "Math"
        if "hóa" in name.lower():
            subject = "Chemistry"
        if "tiếng anh" in name.lower():
            subject = "English"
        if "ngữ văn" in name.lower():
            subject = "Literature"
        if "lịch sử" in name.lower() or "môn sử" in name.lower():
            subject = "History"
        if "sinh học" in name.lower():
            subject = "Biology"
        if "địa lý" in name.lower():
            subject = "Geography"
        # print(soup)
        link_file = soup.find("object")["data"]
        return grade, subject, name, link_file

    def download_pdf(self):
        self.file = self.link_file.split("/")[-1]
        r = requests.get(self.link_file, stream=True)
        with open(self.file, "wb") as fd:
            fd.write(r.content)
        print("Download successful!")
        self.obj_storage = ObjectStorage(FOLDER, self.name, self.file)
        self.saved_name = self.obj_storage.file_name

    def post_to_storage(self):
        self.obj_storage.post_file()
        pass

    def post_to_api(self):
        data = {"grade_level": self.grade,
                "subject": self.subject,
                "name": self.name,
                "raw_content": self.saved_name,
                "storage_id": self.saved_name.split(".")[0],
                "source_crawler": self.source_crawler}
        print(data)
        r = requests.post(API_ASSIGNMENT_URL, json=data)
        print(r.status_code)
        return r.status_code

    def process(self):
        self.download_pdf()
        status_code = self.post_to_api()
        if status_code == 201:
            self.post_to_storage()
        os.remove(self.file)
        print("Remove file")


def scrapy(first_url):
    req = requests.get(first_url)
    s = BeautifulSoup(req.text, "lxml")
    url_list = s.findAll("a", href=True, style=False, target=False, title=True)[1:]
    # print(SOURCE + random.choice(url_list)["href"])
    for obj in url_list:
        try:
            url = SOURCE + obj["href"]
            moon_crawler = MoonCrawler(url)
            moon_crawler.process()
        except TypeError:
            pass
    scrapy(SOURCE + random.choice(url_list)["href"])


if __name__ == '__main__':
    scrapy(URL)
