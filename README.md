# EduCrawler

## Onluyen Crawler
Các môn học và lớp của ôn luyện:
- TOAN, VATLY, HOAHOC, ANHTHPT2, TIENGANHTHPT3, NGUVAN, SINH, LICHSU, DIALY, GDCD

- C10, C11, C12

Để crawl các bài tự luyện từ ôn luyện sử dụng command: <br>
`python onluyen_crawler.py -s TOAN -g C10 -d data/onluyen `

Để crawl các bài kiểm tra từ ôn luyện sử dụng command: <br>
`python onluyen_crawler_test.py -s TOAN -g C10 -d data/onluyen `

## Hoctot Crawler
Để crawl các bài kiểm tra của học tốt sử dụng command: <br>
`python hoctot_crawler.py -d data/hoctot`

hoctot_crawler sẽ crawl tất cả các bài kiểm tra của 4 môn Toán, Lý, Hóa, Tiếng Anh


## vietjack Crawler
Do có sử dụng API Mathpix để convert từ ảnh sang công thức, do đó trước khi chạy crawl cần export thêm các biến *mathpix_id* và *mathpix_key* để sử dụng.

Để crawl các đề của tất cả các môn, sử dụng command: <br>
`python3 vietjack_new_crawler.py`

## hocmai Crawler
Do cần phải đăng nhập vào trang web để có thể lấy được dữ liệu từ link ảnh, nên cần phải thêm cookie đã đăng nhập vào thư mục *config/constant* và sửa đổi HOCMAI_HEADERS tương ứng.

Để crawl các đề của tất cả các môn, sử dụng command: <br>
`python3 hocmaivn_crawler.py`


### Do crawl sử dụng selenium, các website có limit về thời gian đăng nhập, và số thiết bị đăng nhập, do đó sẽ có khả năng không đăng nhập đươc, sẽ cần chờ một khoảng thời gian nghỉ để chạy lại code crawl