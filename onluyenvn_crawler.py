from __future__ import print_function

import os

import requests
from bs4 import BeautifulSoup

from helper.google_drive import download_file
from helper.object_storage import ObjectStorage
from config.constant import API_ASSIGNMENT_URL

FOLDER = "onluyen"


class OnLuyenCrawler:
    def __init__(self, url, g, subj, n, google_id, f_name):
        self.source_crawler = url
        self.saved_name = None
        self.obj_storage = None
        self.file = None
        self.grade, self.subject, self.name = g, subj, n
        self.gg_id, self.file_name = google_id, f_name

    def get_data(self):
        self.file = download_file(self.gg_id, self.file_name)
        self.obj_storage = ObjectStorage(FOLDER, name, self.file)
        self.saved_name = self.obj_storage.file_name

    def post_to_storage(self):
        self.obj_storage.post_file()
        pass

    def post_to_api(self):
        data = {"grade_level": self.grade,
                "subject": self.subject,
                "name": self.name,
                "raw_content": self.saved_name,
                "storage_id": self.saved_name.split(".")[0],
                "source_crawler": self.source_crawler}
        r = requests.post(API_ASSIGNMENT_URL, json=data)
        print(r.json())
        print(r.status_code)
        return r.status_code

    def process(self):
        try:
            self.get_data()
        except Exception as e:
            print(e)
        status_code = self.post_to_api()
        if status_code == 201:
            self.post_to_storage()
        os.remove(self.file)
        print("Remove file")


if __name__ == '__main__':
    grade_list = ["de-thi-thpt-quoc-gia", "lop-12", "lop-11", "lop-10"]
    # grade_list = ["de-thi-thpt-quoc-gia"]
    for obj in grade_list:
        for page in range(1, 394):
            link = "https://www.onluyen.vn/thu-vien-tai-lieu/{}/page/{}/".format(obj, page)
            print(link)

            req = requests.get(link)
            soup = BeautifulSoup(req.text, "lxml")

            containers = soup.find_all("article")
            for container in containers[:]:
                name = container.find("h5", {"class": "entry-title"}).text
                print(name)
                info = container.find("div", {"class": "entry-cat"}).text
                grade, subject = 12, "Math"

                # get grade
                if "thpt quốc gia" in info.lower() or "lớp 12" in info.lower() \
                        or " 12 " in info.lower() or "Đề thi đại học" in info.lower():
                    grade = 12
                if "lớp 11" in info.lower() or " 11 " in info.lower():
                    grade = 11
                if "lớp 10" in info.lower() or " 10 " in info.lower():
                    grade = 10

                # get subject
                if "vật lý" in info.lower() or "vật lí" in info.lower():
                    subject = "Physics"
                if "toán" in info.lower():
                    subject = "Math"
                if "hóa" in info.lower():
                    subject = "Chemistry"
                if "tiếng anh" in info.lower():
                    subject = "English"
                if "ngữ văn" in info.lower():
                    subject = "Literature"
                if "lịch sử" in info.lower() or "môn sử" in info.lower():
                    subject = "History"
                if "sinh học" in info.lower():
                    subject = "Biology"
                if "địa lý" in info.lower() or "địa lí" in info.lower():
                    subject = "Geography"
                if "giáo dục công dân" in info.lower():
                    subject = "GDCD"

                if "id=" in container.find("a", download=True)["href"]:
                    gg_id = container.find("a", download=True)["href"].split("id=")[-1]
                else:
                    gg_id = container.find("a", download=True)["href"].split("/")[-1]
                file_name = container.find("a", {"class": "readmore"})["href"].split("/")[-2]

                on_luyen_crawler = OnLuyenCrawler(link, grade, subject, name, gg_id, file_name)
                on_luyen_crawler.process()
