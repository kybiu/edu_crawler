import hashlib
import os
import time
import json

import requests
from bs4 import BeautifulSoup

from config.constant import HEADERS, API_ASSIGNMENT_URL
from helper.object_storage import ObjectStorage

FOLDER = "hanoi-study"


class HanoiStudyCrawler:
    def __init__(self, url, assignment_name):
        self.source_crawler = url
        self.assignment_name = assignment_name
        self.folder_name = self.create_folder_name()
        self.object_storage = None
        self.data = self.get_info()

    def create_folder_name(self):
        h = hashlib.sha256()
        h_str = (self.assignment_name + str(time.time())).encode('utf-8')
        h.update(h_str)
        return "{}".format(h.hexdigest())

    def process_img(self, img_link):
        img = img_link.split("/")[-1]
        r = requests.get(img_link, stream=True)
        with open(img, "wb") as fd:
            fd.write(r.content)
        self.object_storage = ObjectStorage("{}/{}".format(FOLDER, self.folder_name), img, img)
        self.object_storage.post_file()
        os.remove(img)
        return self.object_storage.file_name

    def get_info(self):
        grade, subject = 12, "Math"

        # get grade
        if "thpt quốc gia" in self.assignment_name.lower() or "lớp 12" in self.assignment_name.lower() \
                or " 12 " in self.assignment_name.lower():
            grade = 12
        if "lớp 11" in self.assignment_name.lower() or " 11 " in self.assignment_name.lower():
            grade = 11
        if "lớp 10" in self.assignment_name.lower() or " 10 " in self.assignment_name.lower():
            grade = 10

        # get subject
        if "vật lý" in self.assignment_name.lower() or "vật lí" in self.assignment_name.lower():
            subject = "Physics"
        if "toán" in self.assignment_name.lower():
            subject = "Math"
        if "hóa" in self.assignment_name.lower():
            subject = "Chemistry"
        if "tiếng anh" in self.assignment_name.lower():
            subject = "English"
        if "ngữ văn" in self.assignment_name.lower():
            subject = "Literature"
        if "lịch sử" in self.assignment_name.lower() or "môn sử" in self.assignment_name.lower():
            subject = "History"
        if "sinh học" in self.assignment_name.lower():
            subject = "Biology"
        if "địa lý" in self.assignment_name.lower() or "địa lí" in self.assignment_name.lower():
            subject = "Geography"

        r = requests.get(self.source_crawler, headers=HEADERS)
        soup = BeautifulSoup(r.text, "lxml")

        # process sub
        sub_list = soup.findAll("sub")
        if len(sub_list) != 0:
            for sub in sub_list:
                sub.replaceWith("_" + sub.text)

        # get data
        q_list = soup.findAll("div", {"class": "question-box"})
        meta_data = []
        for q in q_list[:]:
            print(q)
            q_name = q.find("div", {"class": "question-box-title-number"}).text
            q_text = q.find("div", {"class": "col-md-11 col-10 question-box-title"}).text
            q_text = " ".join(q_text.split())
            q_img = q.find("img")
            if q_img:
                q_text += self.process_img(q_img["src"])
            option_list = q.findAll("div", {"class": "col-lg-6 col-md-12 splip-answer"})
            ops = [q_text]
            for op in option_list:
                ops.append(' '.join(op.text.split()))
            q_text = "\n".join(ops)
            print(q_text)
            meta_data.append({
                "name": q_name,
                "text_content": q_text,
                "source_crawler": self.source_crawler,
                "answer": None,
            })
        if len(meta_data) == 0:
            return
        data = {
            "grade_level": grade,
            "subject": subject,
            "name": "[hn-study] " + self.assignment_name,
            "storage_id": self.folder_name,
            "source_crawler": self.source_crawler,
            "meta_data": meta_data
        }
        return data

    def post_to_api(self):
        final_r = requests.post(API_ASSIGNMENT_URL, json=self.data)
        return final_r.content
        # pass


if __name__ == '__main__':
    for i in range(0, 10):
        re = requests.get("http://study.hanoi.edu.vn/home/getexamlisthome?page={}"
                          "&departmentTypeCode=02&schoolCode=&schoolLevelCode=04&"
                          "schoolBlockCode=11&subjectCode=&chapterCode=&_=1608628528377".format(i))
        containers = json.loads(re.text)
        if len(containers) != 0:
            for container in containers:
                print("http://study.hanoi.edu.vn/lam-bai/{}-{}".format(container["nameUrl"], container["id"]))
                hn_crawler = HanoiStudyCrawler(
                    "http://study.hanoi.edu.vn/lam-bai/{}-{}".format(container["nameUrl"], container["id"]),
                    container["name"])
                print(hn_crawler.post_to_api())
            #     break
            # break
