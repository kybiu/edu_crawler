import hashlib
import json
import os
import re
import time

import requests
import pandas as pd
from bs4 import BeautifulSoup

from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from config.constant import HEADERS, API_ASSIGNMENT_URL, HOCMAI_HEADERS
from helper.object_storage import ObjectStorage

FOLDER = "hocmaivn"


class HocMaiCrawler:
    def __init__(self, grades):
        self.list_grade = grades
        self.grade_map = {'lớp 12': '13', 'lớp 11': '12', 'lớp 10': '11'}
        self.subject_map = {'tiếng anh': '6', 'toán': '5', 'ngữ văn': '3', 'vật lí': '7', 'hóa học': '8',
                            'sinh học': '9', 'lịch sử': '10', 'địa lý': '11', 'GDCD': '12'}
        self.assignment_name = ''
        self.url = 'https://hocmai.vn/kiem-tra-thi-thu/'
        self.browser = None
        self.object_storage = None
        self.count = 0

    def create_folder_name(self):
        h = hashlib.sha256()
        h_str = (self.assignment_name + str(time.time())).encode('utf-8')
        h.update(h_str)
        return "{}".format(h.hexdigest())

    def process_img(self, img_link):
        img = img_link.split("/")[-1]
        try:
            r = requests.get(img_link, stream=True, headers=HOCMAI_HEADERS)
            if r.status_code == 200:
                with open(img, "wb") as fd:
                    fd.write(r.content)
            else:
                print('Error save image')
        except Exception as e:
            print("Error get image")
            print(str(e))
        self.object_storage = ObjectStorage("{}/{}".format(FOLDER, self.create_folder_name()), img, img)
        self.object_storage.post_file()
        os.remove(img)
        return self.object_storage.file_name

    def start_webdriver(self, url):
        # setting config visual
        firefox_profile = FirefoxProfile()
        # Disable CSS
        firefox_profile.set_preference('permissions.default.stylesheet', 2)
        # Disable images
        firefox_profile.set_preference('permissions.default.image', 2)
        # Disable Flash
        firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')

        self.browser = webdriver.Firefox(firefox_profile=firefox_profile)
        self.login()
        self.browser.get(url)

    def login(self):
        self.browser.get('https://hocmai.vn')

        self.browser.find_element_by_class_name('btn-login').click()
        time.sleep(1)
        self.browser.find_element_by_name('username').clear()
        self.browser.find_element_by_name('username').send_keys('workftech2020@gmail.com')
        self.browser.find_element_by_name('password').clear()
        self.browser.find_element_by_name('password').send_keys('ftech@123')
        self.browser.find_element_by_class_name('register-btn').submit()

    def get_contest_info(self, link):
        # access page
        self.browser.get(link)
        if self.browser.find_elements_by_class_name('print_subject_anwser_guide'):
            link_solution = self.browser.find_element_by_class_name('print_subject_anwser_guide').get_attribute('href')
        elif self.browser.find_elements_by_class_name('print_subject_anwser'):
            link_solution = self.browser.find_element_by_class_name('print_subject_anwser').get_attribute('href')
        else:
            link_solution = self.browser.find_element_by_class_name('print_subject_guide').get_attribute('href')

        link_origin = ''
        if self.browser.find_element_by_class_name('print_subject_guide'):
            link_origin = self.browser.find_element_by_class_name('print_subject_guide').get_attribute('href')
        self.browser.get(link_solution)
        time.sleep(1)

        # convert to beautifulsoup
        page = BeautifulSoup(self.browser.page_source, "lxml")
        main_body = page.find('div', {'class': 'result-page'})

        # normalize
        # text
        for obj1 in main_body.findAll('span', {'class': 'mjx-chtml MJXc-display'}):
            obj1.string = ' '
        for obj2 in main_body.findAll('span', {'class': 'MathJax_Preview'}):
            obj2.string = ' '

        # image
        for img in main_body.findAll("img"):
            link_image = img['src'].replace('../', 'https://hocmai.vn/').replace('http://', 'https://')
            saved_image = self.process_img(link_image)
            p_tag = page.new_tag('p')
            p_tag.string = ' ' + saved_image + ' '
            img.insert_after(p_tag)
            img.unwrap()

        # equation
        for e in main_body.find_all('script', type=re.compile("math/tex")):
            e.string = f" $ {e.text} $ "

        # sub
        for sub in main_body.findAll('sub'):
            # sub.replaceWith("_" + sub.text)
            sub.string = f"_{sub.text}"

        # sup
        for sup in main_body.findAll('sup'):
            sup.string = f"^{sup.text}"

        # underline
        for u in main_body.find_all('u'):
            if u.text:
                u.string = "<u>" + u.text + "</u>"

        for ins in main_body.find_all('ins'):
            if ins.text:
                ins.string = "<u>" + ins.text + "</u>"

        for st in main_body.find_all('span', style=re.compile("text-decoration: underline")):
            if st.text:
                st.string = "<u>" + st.text + "</u>"

        # get info for each contest
        questions = main_body.find_all('div', {"class": "que"})

        meta_data = []
        for q in questions:
            try:
                # find info box
                qtex_box = q.find('div', {'class': 'qtext'})
                ans_box = q.find('div', {'class': 'ablock'})

                # check has solution
                solution = ''
                check_solution = q.find('div', {'class': 'generalfeedback'})
                if check_solution:
                    solution = check_solution.text

                # check has correct answer
                correct_ans = ''
                if ans_box.findAll('td', {'class': 'c1 text highlight'}):
                    correct_ans = ans_box.find('td', {'class': 'c1 text highlight'}).text

                # data
                text_content = '\n'.join([t.strip() for t in qtex_box.text.split('\n ')])
                text_answer = '\n'.join([t.strip() for t in ans_box.text.split('\n ')])
                correct_answer = correct_ans
                order = q.find('div', {'class': 'info'}).text
                question_type = ""

                if link_origin:
                    link_solution = link_origin
                meta_data.append({
                    "name": order,
                    "raw_question": text_content + ';' + text_answer + ';' + solution,
                    "text_content": text_content,
                    "text_answer": text_answer,
                    "correct_answer": correct_answer,
                    "solution": solution,
                    "source_crawler": link_solution,
                    "question_type": question_type,
                })

            except Exception as e:
                print('Error {} in contest: {}'.format(str(e), link_solution))
                continue

        return link_solution, meta_data

    def get_contest_of_subject(self, url, grade, subject):
        # go search page
        self.browser.get(url)

        # choose grade, subject and search
        grade_index = self.grade_map.get(grade)
        self.browser.find_element_by_xpath('//*[@id="ktth-class-choose"]').click()
        self.browser.find_element_by_xpath(
            f'//*[@id="ktth-content"]/div/div[2]/div[2]/form/div/div[1]/div[1]/ul/li[{grade_index}]/a').click()

        subject_index = self.subject_map.get(subject)
        self.browser.find_element_by_xpath('//*[@id="ktth-subject-choose"]').click()
        self.browser.find_element_by_xpath(
            f'//*[@id="ktth-content"]/div/div[2]/div[2]/form/div/div[1]/div[3]/ul/li[{subject_index}]/a').click()

        self.browser.find_element_by_xpath('//*[@id="ktth-content"]/div/div[2]/div[2]/form/div/div[2]/input').click()

        # get list contest of subject
        label_contests = self.browser.find_elements_by_xpath('//*[@id="ktth-content"]/div/div[4]/div')
        if not label_contests:
            print('Not data in ', subject, grade)
            return
        list_contest = []
        for n, i in enumerate(label_contests):
            quizs = self.browser.find_elements_by_xpath(
                f'//*[@id="ktth-content"]/div/div[4]/ul[{n + 1}]/li/div/div[1]/a')
            links = [li.get_attribute('href') for li in quizs]
            titles = [t.text for t in quizs]
            tmp = [{'title': titles[j] + ' _ ' + i.text.strip(), 'link': links[j]} for j in range(len(quizs))]
            list_contest += tmp

        data_df = []
        for i, c in enumerate(list_contest):
            try:
                self.assignment_name = c.get('title')
                print('***************')
                # print(f'number saved contest of {subject}: ', i)
                # print('title: ', c.get('title'))
                link_solution, meta_data = self.get_contest_info(c.get('link'))
                # print('meta-data: ', meta_data)
                if len(meta_data) == 0:
                    continue

                # get subject
                if subject == "vật lí":
                    subject = "Physics"
                if subject == "toán":
                    subject = "Math"
                if subject == "hóa học":
                    subject = "Chemistry"
                if subject == "tiếng anh":
                    subject = "English"
                if subject == "ngữ văn":
                    subject = "Literature"
                if subject == "lịch sử":
                    subject = "History"
                if subject == "sinh học":
                    subject = "Biology"
                if subject == "địa lý":
                    subject = "Geography"

                for info in meta_data:
                    tmp = [int(grade.split(' ')[-1]), subject, None, None, info.get('name'), info.get('raw_question'),
                           info.get('text_content'), info.get('text_answer'), info.get('correct_answer'),
                           info.get('solution'), info.get('question_type'), info.get('source_crawler'), None]
                    data_df.append(tmp)

                df = pd.DataFrame(data_df)
                if len(data_df) > 0:
                    df.columns = ['grade_level', 'subject', 'chapter', 'lesson', 'name', 'raw_question', 'text_content',
                                  'text_answer', 'correct_answer', 'solution', 'question_type', 'source_crawler',
                                  'source_detail']
                    if not os.path.exists('data/hocmai/'):
                        os.mkdir('data/hocmai/')
                    df.to_csv(f'data/hocmai/data_{grade[-2:]}_{subject}.csv', index=False)
                else:
                    print(f'not data in subject {subject}, {grade}')

                data = {
                    "grade_level": int(grade.split(' ')[-1]),
                    "subject": subject,
                    "chapter": None,
                    "lesson": None,
                    "title": "[hocmaivn] " + c.get('title'),
                    "set_title": c.get('title').split('_')[0].strip(),
                    "storage_id": self.create_folder_name(),
                    "source_crawler": link_solution,
                    "meta_data": meta_data
                }

                # add in db
                res = requests.post(API_ASSIGNMENT_URL, json=data)
                # self.count += 1
                # print(res.status_code)
                # print('Saved Contest : ', str(self.count))

            except Exception as e:
                print("Exception in : ", str(e), subject, grade)
                continue

        return

    def get_link_quiz(self):
        try:
            # open web driver
            self.start_webdriver(self.url)

            for grade in self.list_grade:
                # if grade == 'lớp 10':
                #     list_subject = ['tiếng anh', 'ngữ văn']
                # elif grade == 'lớp 11':
                #     list_subject = ['tiếng anh']
                # elif grade == 'lớp 12':
                #     list_subject = ['toán', 'vật lí', 'hóa học', 'sinh học', 'tiếng anh']
                # else:
                #     return
                list_subject = ['tiếng anh', 'toán', 'ngữ văn', 'vật lí', 'hóa học', 'sinh học', 'lịch sử', 'địa lý',
                                'GDCD']
                for s in list_subject:
                    self.get_contest_of_subject(self.url, grade, s)
        except Exception as e:
            print('Exception : ', str(e))

        self.browser.quit()
        return


if __name__ == '__main__':
    list_grade = ['lớp 12', 'lớp 11', 'lớp 10']
    crawler = HocMaiCrawler(list_grade)
    crawler.get_link_quiz()
    print('***** Success crawl *****')
