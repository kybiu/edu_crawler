import os

import requests
from pathlib import Path
from bs4 import BeautifulSoup

from helper.object_storage import ObjectStorage
from config.constant import API_ASSIGNMENT_URL

URL = "https://toanmath.com/2020/12/de-thi-thu-tot-nghiep-thpt-2021-lan-1-mon-toan-truong-ly-thai-to-bac-ninh.html"
BASE_DIR = Path(__file__).resolve().parent.parent
SOURCE = "https://toanmath.com"
FOLDER = "toanmath"


class ToanMathCrawler:
    def __init__(self, url):
        self.source_crawler = url
        self.grade, self.subject, self.name, self.link_file = self.get_info()
        self.saved_name = None
        self.obj_storage = None
        self.file = None

    def get_info(self):
        r = requests.get(self.source_crawler)
        soup = BeautifulSoup(r.text, "lxml")
        # print(soup)
        name = soup.find("title").text.strip()
        name = " ".join(name.split())
        grade, subject = 12, "Math"

        # get grade
        if "thpt quốc gia" in name.lower() \
                or "lớp 12" in name.lower() \
                or "tốt nghiệp thpt" in name.lower():
            grade = 12
        if "lớp 11" in name.lower():
            grade = 11
        if "lớp 10" in name.lower():
            grade = 10

        # get subject
        if "vật lý" in name.lower() or "vật lí" in name.lower():
            subject = "Physics"
        if "toán" in name.lower():
            subject = "Math"
        if "hóa" in name.lower():
            subject = "Chemistry"
        if "tiếng anh" in name.lower():
            subject = "English"
        if "ngữ văn" in name.lower():
            subject = "Literature"
        if "lịch sử" in name.lower() or "môn sử" in name.lower():
            subject = "History"
        if "sinh học" in name.lower():
            subject = "Biology"
        if "địa lý" in name.lower():
            subject = "Geography"

        link_file = None
        for i in soup.findAll("a", href=True):
            if ".pdf" in i["href"]:
                link_file = i["href"]
        print(link_file)
        return grade, subject, name, link_file

    def download_pdf(self):
        self.file = self.link_file.split("/")[-1]
        r = requests.get(self.link_file, stream=True)
        with open(self.file, "wb") as fd:
            fd.write(r.content)
        print("Download successful!")
        self.obj_storage = ObjectStorage(FOLDER, self.name, self.file)
        self.saved_name = self.obj_storage.file_name

    def post_to_storage(self):
        self.obj_storage.post_file()
        pass

    def post_to_api(self):
        data = {"grade_level": self.grade,
                "subject": self.subject,
                "name": self.name,
                "raw_content": self.saved_name,
                "storage_id": self.saved_name.split(".")[0],
                "source_crawler": self.source_crawler}
        print(data)
        r = requests.post(API_ASSIGNMENT_URL, json=data)
        print(r.status_code)
        return r.status_code

    def process(self):
        self.download_pdf()
        # status_code = 200
        status_code = self.post_to_api()
        if status_code == 201:
            self.post_to_storage()
        os.remove(self.file)
        print("Remove file")


def scrapy():
    url_list = []
    for i in range(1, 83):
        url = "{}/{}".format("https://toanmath.com/de-thi-thu-mon-toan/page", i)
        req = requests.get(url)
        s = BeautifulSoup(req.text, "lxml")
        for j in s.findAll("a", href=True, title=True, rel=True)[1:-1]:
            print(j["href"])
            url_list.append(j["href"])

    for url in url_list:
        try:
            toan_math_crawler = ToanMathCrawler(url)
            toan_math_crawler.process()
        except Exception as e:
            print(e)


class ToanMathCrawler2:
    def __init__(self, url):
        self.source_crawler = url
        self.grade, self.subject, self.name, self.link_file = self.get_info()
        self.saved_name = None
        self.obj_storage = None
        self.file = None

    def get_info(self):
        name = self.source_crawler.split("/")[-1].split(".")[0]
        name = " ".join(name.split("-"))
        grade, subject = 12, "Math"

        # get grade
        if "thpt quoc gia" in name.lower() \
                or " 12 " in name.lower() \
                or "tot nghiep thpt" in name.lower():
            grade = 12
        if " 11 " in name.lower():
            grade = 11
        if " 10 " in name.lower():
            grade = 10

        # get subject
        if "vật lý" in name.lower() or "vật lí" in name.lower():
            subject = "Physics"
        if "toán" in name.lower():
            subject = "Math"
        if "hóa" in name.lower():
            subject = "Chemistry"
        if "tiếng anh" in name.lower():
            subject = "English"
        if "ngữ văn" in name.lower():
            subject = "Literature"
        if "lịch sử" in name.lower() or "môn sử" in name.lower():
            subject = "History"
        if "sinh học" in name.lower():
            subject = "Biology"
        if "địa lý" in name.lower():
            subject = "Geography"

        link_file = self.source_crawler

        return grade, subject, name, link_file

    def download_file(self):
        self.file = self.link_file.split("/")[-1]
        r = requests.get(self.link_file, stream=True)
        with open(self.file, "wb") as fd:
            fd.write(r.content)
        print("Download successful!")
        self.obj_storage = ObjectStorage(FOLDER, self.name, self.file)
        self.saved_name = self.obj_storage.file_name

    def post_to_storage(self):
        self.obj_storage.post_file()
        pass

    def post_to_api(self):
        data = {"grade_level": self.grade,
                "subject": self.subject,
                "name": self.name,
                "raw_content": self.saved_name,
                "storage_id": self.saved_name.split(".")[0],
                "source_crawler": self.source_crawler}
        print(data)
        r = requests.post(API_ASSIGNMENT_URL, json=data)
        print(r.status_code)
        return r.status_code

    def process(self):
        self.download_file()
        # status_code = 200
        status_code = self.post_to_api()
        if status_code == 201:
            self.post_to_storage()
        os.remove(self.file)
        print("Remove file")


if __name__ == '__main__':
    scrapy()
    # req_1 = requests.get("https://toanmath.com/data/")
    # s_1 = BeautifulSoup(req_1.text, "lxml")
    # for link in s_1.findAll("a", href=True)[6:]:
    #     if link["href"].split(".")[-1] != "rar":
    #         toan_math_crawler_2 = ToanMathCrawler2("{}{}".format(SOURCE, link["href"]))
    #         toan_math_crawler_2.process()
