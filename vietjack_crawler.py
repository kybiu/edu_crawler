import hashlib
import os
import time
from config.constant import API_ASSIGNMENT_URL
import requests
from bs4 import BeautifulSoup

from helper.object_storage import ObjectStorage

SOURCE_CRAWLER = "https://vietjack.com/de-kiem-tra-lop-12/de-thi-hoc-ki-2-vat-li-12-de-4.jsp"
FOLDER = "vietjack"


class VietJackCrawler:
    def __init__(self, url):
        self.source_crawler = url
        self.folder_name = self.create_folder_name()
        self.grade, self.subject, self.name, self.content, self.soup = self.get_info()
        self.save_name = None
        self.object_storage = None
        self.file = None

    def get_info(self):
        r = requests.get(self.source_crawler)
        soup = BeautifulSoup(r.text, "lxml")
        content = soup.find("div", class_="content")
        name = soup.find("h1", class_="title").text.strip()
        name = " ".join(name.split())
        grade, subject = None, None

        # get grade
        if "thpt quốc gia" in name.lower() or "lớp 12" in name.lower() or "12" in name.lower():
            grade = 12
        if "lớp 11" in name.lower() or "11" in name.lower():
            grade = 11
        if "lớp 10" in name.lower() or "10" in name.lower():
            grade = 10

        # get subject
        if "vật lý" in name.lower() or "vật lí" in name.lower():
            subject = "Physics"
        if "toán" in name.lower():
            subject = "Math"
        if "hóa" in name.lower():
            subject = "Chemistry"
        if "tiếng anh" in name.lower():
            subject = "English"
        if "ngữ văn" in name.lower():
            subject = "Literature"
        if "lịch sử" in name.lower() or "môn sử" in name.lower():
            subject = "History"
        if "sinh học" in name.lower():
            subject = "Biology"
        if "địa lý" in name.lower():
            subject = "Geography"

        return grade, subject, name, content, soup

    def create_folder_name(self):
        h = hashlib.sha256()
        h_str = (self.source_crawler.split("/")[-1].split(".")[0] + str(time.time())).encode('utf-8')
        h.update(h_str)
        return "{}".format(h.hexdigest())

    def process_img(self, img_link):
        img = img_link.split("/")[-1]
        r = requests.get(img_link, stream=True)
        with open(img, "wb") as fd:
            fd.write(r.content)
        self.object_storage = ObjectStorage("{}/{}".format(FOLDER, self.folder_name), img, img)
        self.object_storage.post_file()
        os.remove(img)
        return self.object_storage.file_name

    def process_questions_content(self):
        # replace img tag by str
        img_list = self.content.findAll("img")
        if len(img_list) != 0:
            for img in img_list:
                # latex_content = covert_image_to_latex(img["src"].replace("../", "https://vietjack.com/"))
                # img.replace_with(latex_content)
                # img.replace_with(img["src"].replace("../", "https://vietjack.com/"))
                p_tag = self.soup.new_tag("p")
                img_link = img["src"].replace("../", "https://vietjack.com/")
                saved_img = ""
                if "teacher" not in img_link and ".jpg" not in img_link:
                    saved_img = " " + self.process_img(img_link) + " "
                p_tag.string = saved_img
                img.insert_after(p_tag)
                img.unwrap()

        qa_list = self.content.findAll('p', )[2:-19]
        question_list = []
        answer_list = []
        question_answer = ""

        for i in qa_list:
            b_tag = i.find("b")
            if b_tag:
                if question_answer != "":
                    if "Đáp án " in question_answer:
                        answer_list.append(question_answer)
                    else:
                        if "Thời gian làm bài" not in question_answer \
                                and "4 đề thi giữa kì 1" not in question_answer:
                            question_list.append(question_answer)
                name = b_tag.text
                question_answer = name + "\n" + i.text.replace(name + " ", "") + "\n"
            else:
                if "Xem thêm các" not in i.text and "4 đề thi giữa kì 1" not in i.text \
                        and "Thời gian làm bài" not in i.text:
                    question_answer += i.text

        # assert (len(question_list) - 1 == len(answer_list))

        meta_data = []

        for i in range(len(answer_list)):
            obj = {}
            name = question_list[i].split("\n")[0]
            question = question_list[i].replace(name + "\n", "")
            answer = answer_list[i].replace(name + "\n", "")
            obj["name"] = name
            obj["text_content"] = question
            obj["source_crawler"] = SOURCE_CRAWLER
            obj["answer"] = answer
            meta_data.append(obj)

        return meta_data


vjack_crawler = VietJackCrawler(SOURCE_CRAWLER)
# print(vjack_crawler.grade)
# print(vjack_crawler.name)
# print(vjack_crawler.subject)
# print(vjack_crawler.folder_name)
meta_data = vjack_crawler.process_questions_content()


data = {
    "grade_level": vjack_crawler.grade,
    "subject": vjack_crawler.subject,
    "name": vjack_crawler.name,
    "storage_id": vjack_crawler.folder_name,
    "source_crawler": SOURCE_CRAWLER,
    "meta_data": meta_data
}
print(data)
# requests.post(API_ASSIGNMENT_URL, json=data)
