from __future__ import print_function

import json
import os
import unidecode
import requests
from bs4 import BeautifulSoup

from helper.google_drive import download_file
from helper.object_storage import ObjectStorage
from config.constant import API_ASSIGNMENT_URL

FOLDER = "mathvn"


class MathVNCrawler:
    def __init__(self, url):
        self.source_crawler = url
        self.saved_name = None
        self.obj_storage = None
        self.file = None
        self.grade, self.subject, self.original_name, self.gg_id, self.file_name = self.get_info()

    def get_info(self):
        r = requests.get(self.source_crawler)
        soup = BeautifulSoup(r.text, "lxml")
        title = soup.find("title").text.strip()
        grade, subject = 12, "Math"

        # get grade
        if "thpt quốc gia" in title.lower() or "lớp 12" in title.lower() \
                or " 12 " in title.lower() or "Đề thi đại học" in title.lower():
            grade = 12
        if "lớp 11" in title.lower() or " 11 " in title.lower():
            grade = 11
        if "lớp 10" in title.lower() or " 10 " in title.lower():
            grade = 10

        gg_id = soup.find("iframe", height=True, width=True)["src"].split("/")[-2]

        file_name = "-".join(unidecode.unidecode(title).split(" "))
        print(file_name)

        return grade, "Math", title, gg_id, file_name

    def get_data(self):
        self.file = download_file(self.gg_id, self.file_name)
        self.obj_storage = ObjectStorage(FOLDER, self.file_name, self.file)
        self.saved_name = self.obj_storage.file_name

    def post_to_storage(self):
        self.obj_storage.post_file()
        pass

    def post_to_api(self):
        data = {"grade_level": self.grade,
                "subject": self.subject,
                "name": self.original_name,
                "raw_content": self.saved_name,
                "storage_id": self.saved_name.split(".")[0],
                "source_crawler": self.source_crawler}
        r = requests.post(API_ASSIGNMENT_URL, json=data)
        print(r.json())
        print(r.status_code)
        return r.status_code

    def process(self):
        try:
            self.get_data()
        except Exception as e:
            print(e)
        status_code = self.post_to_api()
        if status_code == 201:
            self.post_to_storage()
        os.remove(self.file)
        print("Remove file")


if __name__ == '__main__':
    for i in range(1, 122):
        print("page " + str(i))
        start_index = 1 if i == 1 else i * 7
        req = requests.get(
            "https://www.mathvn.com/feeds/posts/default/-/%C4%90%E1%BB%81%20thi%20-%20%C4%91%C3%A1p%20%C3%A1n?"
            "alt=json-in-script&max-results=7&start-index={}"
            "&callback=jQuery1111011973906384327626_1609126301488&_=1609126301492".format(
                start_index))
        bs = BeautifulSoup(req.text, "lxml")
        containers = json.loads(bs.find("p").text[59:-2])["feed"]["entry"]
        for container in containers[:]:
            link = container["link"][-1]["href"]
            print(link)
            try:
                mathvn_crawler = MathVNCrawler(link)
                mathvn_crawler.process()
            except Exception as e:
                print(e)
